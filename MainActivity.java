package com.example.adrianridder.project;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    final long DEFAULT_START_TIME_IN_MILLIS = 10000;
    final long DEFAULT_BREAK_START_TIME_IN_MILLIS = 100000;
    TextView mTextViewCountDown, mColon, mWork, mBreak;
    Button mButtonStart, mButtonReset;
    NumberPicker mNPSeconds, mNPMinutes, mNPBreak;

    CountDownTimer mCountDownTimer;

    boolean mTimerRunning, onBreak;

    long mTimeLeftInMillis = DEFAULT_START_TIME_IN_MILLIS;
    int breakNumber = -1;
    int breakNumberCopy = breakNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        onBreak = false;

        mTextViewCountDown = findViewById(R.id.text_view_countdown);
        mColon = findViewById(R.id.colon);
        mWork = findViewById(R.id.work);
        mBreak = findViewById(R.id.breakView);

        mButtonStart = findViewById(R.id.button_start);
        mButtonReset = findViewById(R.id.button_reset);
        mNPSeconds = findViewById(R.id.seconds);
        mNPSeconds.setMaxValue(59);
        mNPSeconds.setMinValue(0);
        mNPSeconds.setValue(0);
        mNPMinutes = findViewById(R.id.minutes);
        mNPMinutes.setMaxValue(59);
        mNPMinutes.setMinValue(0);
        mNPMinutes.setValue(0);
        mNPBreak = findViewById(R.id.breakNumber);
        mNPBreak.setValue(0);
        mNPBreak.setMinValue(1);
        mNPBreak.setMaxValue(5);

        mButtonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNPSeconds.getValue() == 0 && mNPMinutes.getValue() == 0) {
                    throw new AssertionError();
                }
                if (mTimerRunning) {
                    pauseTimer();
                    mButtonReset.setVisibility(View.VISIBLE);

                } else {
                    if (breakNumberCopy == -1){
                        breakNumberCopy = mNPBreak.getValue();
                    }
                    startTimer();

                }
            }
        });

        mButtonReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetTimer();
                mTimerRunning = false;

            }
        });

        updateCountDownText();

    }

        private void startTimer() {
            mTimeLeftInMillis = (mNPSeconds.getValue() + (mNPMinutes.getValue() * 60)) * 1000;
            mCountDownTimer = new CountDownTimer(mTimeLeftInMillis, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {
                    mTimeLeftInMillis = millisUntilFinished;
                    updateCountDownText();
                }

                @Override
                public void onFinish() {
                    if (onBreak == false) {
                        if (breakNumberCopy == 0) {
                            mTimerRunning = false;
                            resetTimer();

                        }
                        else {
                            onBreak = true;
                            mTimeLeftInMillis = DEFAULT_BREAK_START_TIME_IN_MILLIS;
                            mWork.setText("Break!");
                            breakNumberCopy--;
                            startTimer();
                        }
                    }
                    else if (onBreak == true) {

                        onBreak = false;
                        mTimeLeftInMillis = (mNPSeconds.getValue() + (mNPMinutes.getValue() * 60)) * 1000;
                        mWork.setText("Work!");
                        startTimer();
                    }
                }
            }.start();


            mNPMinutes.setVisibility(View.INVISIBLE);
            mNPSeconds.setVisibility(View.INVISIBLE);
            mColon.setVisibility(View.INVISIBLE);
            mNPBreak.setVisibility(View.INVISIBLE);
            mBreak.setVisibility(View.INVISIBLE);
            mTextViewCountDown.setVisibility(View.VISIBLE);

            mTimerRunning = true;
            mButtonStart.setText("Pause");
            mWork.setVisibility(View.VISIBLE);

        }

        private void pauseTimer() {
            mCountDownTimer.cancel();
            mTimerRunning = false;
            mButtonStart.setText("Start");
        }

        private void resetTimer() {
            mTimeLeftInMillis = (mNPSeconds.getValue() + (mNPMinutes.getValue() * 60)) * 1000;
            mCountDownTimer.cancel();
            mNPMinutes.setVisibility(View.VISIBLE);
            mNPSeconds.setVisibility(View.VISIBLE);
            mTextViewCountDown.setVisibility(View.INVISIBLE);
            mButtonStart.setText("Start");
            mColon.setVisibility(View.VISIBLE);
            mWork.setVisibility(View.INVISIBLE);
            mNPBreak.setVisibility(View.VISIBLE);
            mBreak.setVisibility(View.VISIBLE);

            updateCountDownText();
        }

        private void updateCountDownText() {
            int minutes = (int) mTimeLeftInMillis / 1000 / 60;
            int seconds = (int) mTimeLeftInMillis / 1000 % 60;

            String timeLeftFormatted = String.format(Locale.getDefault(), "%02d:%02d", minutes, seconds);
            mTextViewCountDown.setText(timeLeftFormatted);
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
